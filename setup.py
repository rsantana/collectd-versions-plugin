from setuptools import setup, find_packages
import sys, os

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.rst')).read()
NEWS = open(os.path.join(here, 'NEWS.txt')).read()

version = '1.5.0'

install_requires = [
    'pyyaml'
]


setup(name='collectd_versions',
    version=version,
    description="Collectd Plugin retrieve the status of versions",
    classifiers=[
        "Development Status :: 4 - Beta",
        "Intended Audience :: System Administrators",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python",
        "Topic :: System :: Monitoring",
    ],
    keywords='collectd versions error python plugin',
    url = 'https://gitlab.cern.ch/rsantana/collectd-versions-plugin',
    author='Renato Santana',
    author_email='renato.santana@cern.ch',
    maintainer='CERN IT Monitoring',
    maintainer_email='monit-support@cern.ch',
    license='Apache II',
    packages=find_packages('src'),
    package_dir = {'': 'src'},
    include_package_data=True,
    zip_safe=False,
    install_requires=install_requires
)

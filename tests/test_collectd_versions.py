import sys
import os
import pytest
from mock import MagicMock, Mock, PropertyMock, patch

NOTIF_FAILURE = 4
ACTUATORS_DIR = './data'

@pytest.fixture
def collectd_versions():
	collectd = MagicMock()
	with patch.dict('sys.modules', {'collectd': collectd}):
		import collectd_versions
		yield collectd_versions

def test_plugin_registration(collectd_versions):
	collectd_versions.collectd.register_config_assert_called_once_with(collectd_versions.configure_callback)

def test_configure_should_not_fail(collectd_versions):
	collectd_versions.configure_callback(Mock(children = []))

import collectd
import imp
import re
import requests
import uuid

try:
    imp.find_module('yum')
    import yum
    import yum.misc as misc
    yum_available = True
except ImportError:
    yum_available = False

PLUGIN_NAME = 'yum'
PLUGIN_INSTANCE_TRANSACTION_ERRORS = 'transaction_errors'
PLUGIN_INSTANCE_UNFINISHED_TRANSACTIONS = 'unfinished_transactions'
PLUGIN_INSTANCE_SKIPPED_PACKAGES = 'skipped_packages'
PLUGIN_INSTANCE_BROKEN_REPOS = 'broken_repos'
PLUGIN_INSTANCE_MULTILIB_ERRORS = 'multilib_version_errors'
CONFIG_DEFAULT_INTERVAL = -1
DEFAULT_VALUE = 0
MULTILIB_ERROR_PATTERN = re.compile('Error:\s*Multilib')


class YumConfig(object):
    """Class containing the default values for the configuration parameters of the yum collectd plugin"""
    def __init__(self):
        self.interval = CONFIG_DEFAULT_INTERVAL
        self.config_name = uuid.uuid4().hex


def configure_callback(conf):
    config = YumConfig()

    for node in conf.children:
        try:
            key = node.key.lower()
            if key == 'interval':
                interval = int(node.values[0])
                config.interval = interval if interval > 0 else CONFIG_DEFAULT_INTERVAL
        except ValueError as e:
            collectd.warning('%s plugin: [Error] Value error in %s: %s' % (PLUGIN_NAME, node.key, str(e)))
        except Exception as e:
            collectd.warning('%s plugin: [Error] Configuration error in %s: %s' % (PLUGIN_NAME, node.key, str(e)))

    if config.interval > 0:
        collectd.register_read(callback=read_callback, data=config, name=config.config_name, interval=config.interval)
    else:
        collectd.register_read(callback=read_callback, data=config, name=config.config_name)


def dispatch_broken_repos(config, repos, val):
    '''
    Gets all the active repositories from yum and checks
    the metadata to see if they are working

    Returns the number of repositories with wrong metadata (broken)
    '''
    repos_failure = DEFAULT_VALUE

    for repo in repos:
        repo_ok = False

        for url in repo.urls:
            urlext = url + "/" if url[-1] != "/" else url
            urlext = urlext + "repodata/repomd.xml"
            try:
                response = requests.get(urlext, timeout=5)
                if response.status_code < 400:
                    repo_ok = True
                    break
                else:
                    collectd.warning('[Warning] Repo broken: %s - StatusCode: %d' % (urlext, response.status_code))
            except requests.ConnectionError:
                repo_ok = False
                collectd.warning('[Warning] Repo broken: %s - ConnectionError' % (urlext))

        if repo_ok == False:
            repos_failure += 1

    val.dispatch(plugin=PLUGIN_NAME, plugin_instance=PLUGIN_INSTANCE_BROKEN_REPOS, type='gauge', values=[repos_failure], interval=config.interval)


def dispatch_multilib_version_errors(config, val):
    '''
    Gets the latest logs from distrosync and searchs for the multilib
    version error pattern.

    Returns the number of times that pattern was found
    '''
    multilib_version_error = DEFAULT_VALUE

    with open('/var/log/distro_sync.log', 'r') as distrosync_file:
        for line in reversed(distrosync_file.readlines()): # Does reverse to start from the newest logs
            if MULTILIB_ERROR_PATTERN.search(line):
                multilib_version_error += 1
            elif "Loaded plugins:" in line: # This points the first log line of distro sync
                break

    val.dispatch(plugin=PLUGIN_NAME, plugin_instance=PLUGIN_INSTANCE_MULTILIB_ERRORS,
                 type='gauge', values=[multilib_version_error], interval=config.interval)


def dispatch_skipped_packages(config, last, val):
    '''
    Checks the last transaction available in the yum history
    for skipped packages.

    Returns the number of skipped packages registered in the last transaction.
    '''
    skipped_packages = DEFAULT_VALUE

    if last:
        skipped_packages = len(last.trans_skip)

    val.dispatch(plugin=PLUGIN_NAME, plugin_instance=PLUGIN_INSTANCE_SKIPPED_PACKAGES,
                 type='gauge', values=[skipped_packages], interval=config.interval)


def dispatch_yum_transaction_errors(config, last, val):
    '''
    Checks the last transaction available in the yum history
    for generic transaction errors.

    Returns the number of errors registered in the last transaction.
    '''
    transaction_errors = DEFAULT_VALUE

    if last:
        transaction_errors = len(last.errors)

    val.dispatch(plugin=PLUGIN_NAME, plugin_instance=PLUGIN_INSTANCE_TRANSACTION_ERRORS,
                 type='gauge', values=[transaction_errors], interval=config.interval)


def dispatch_yum_unfinished_transactions(config, val):
    '''
    Checks the yum lib path ('/var/lib/yum' by default) for unfinshed transactions

    Returns the number of unfinished transactions in said directory.
    '''
    unfinished_transactions = len(misc.find_unfinished_transactions())

    val.dispatch(plugin=PLUGIN_NAME, plugin_instance=PLUGIN_INSTANCE_UNFINISHED_TRANSACTIONS,
                 type='gauge', values=[unfinished_transactions], interval=config.interval)


def read_callback(config):
    """Makes val dispatch for the last yum transaction state"""
    if yum_available:
        try:
            try:
                yb = yum.YumBase()
                repos = yb.repos.listEnabled()
                history = yb.history
                try:
                    last = history.last()
                finally:
                    history.close()
            finally:
                yb.close()

            val = collectd.Values()

            dispatch_broken_repos(config, repos, val)
            dispatch_skipped_packages(config, last, val)
            dispatch_yum_transaction_errors(config, last, val)
            dispatch_multilib_version_errors(config, val)
            dispatch_yum_unfinished_transactions(config, val)
        except Exception as e:
            collectd.warning("Yum plugin couldn't handlle the connection with yum: " + str(e))


if yum_available:
    collectd.register_config(configure_callback)
else:
    collectd.warning('Yum not available in the machine')

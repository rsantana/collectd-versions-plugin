# Created by pyp2rpm-3.3.2
%global pypi_name collectd_versions

Name:           python-%{pypi_name}
Version:        1.5.0
Release:        1%{?dist}
Summary:        Collectd Plugin to retrieve the versions status

License:        Apache II
URL:            https://gitlab.cern.ch/monitoring/collectd-renato-plugin
Source0:        https://gitlab.cern.ch/monitoring/collectd-renato-plugin/-/archive/%{version}/collectd-versions-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  python2-devel
BuildRequires:  python-setuptools

%description

%package -n python2-%{pypi_name}
Summary:        %{summary}
%{?python_provide:%python_provide python2-%{pypi_name}}

%description -n python2-%{pypi_name}

%prep
%autosetup -n collectd-versions-%{version}
# Remove bundled egg-info
rm -rf %{pypi_name}.egg-info

%build
%{__python} setup.py build

%install
%{__rm} -rf %{buildroot}
%{__python} setup.py install --skip-build --root %{buildroot}

%clean
%{__rm} -rf %{buildroot}

%post -n python2-%{pypi_name}
/sbin/service collectd condrestart >/dev/null 2>&1 || :

%postun -n python2-%{pypi_name}
if [ $1 -eq 0 ]; then
    /sbin/service collectd condrestart >/dev/null 2>&1 || :
fi

%files -n python2-%{pypi_name}
%doc README.rst NEWS.txt LICENSE
%{python_sitelib}/%{pypi_name}
%{python_sitelib}/%{pypi_name}-%{version}-py?.?.egg-info

%changelog
* Thu Feb 20 2020 Renato Santana <renato.santana@cern.ch> - 1.0.0-1
- Initial package.

Yum Collectd Plugin
============================

Metrics
-------

Currently the versions collectd plugin reports three possible metrics: (to be fixed with  versions metrics!!)
  - broken_repos: Checks if the metadata for all active reports is available, reports the ones that are not
  - skipped_packages: Checks in the last transaction if there was any package skipped and reports the number
  - multilib_version_errors: Checks in the distro_sync.log file for the ocurrence of this error in the last run
  - transaction_errors: Checks in the last transaction if there was any error and reports the number

Configuration
-------------

Example::

    <Plugin "python">

      Import "collectd_versions"

      <Module "collectd_versions">
        Interval "300"
      </Module>

    </Plugin>


* ``Interval``: interval in seconds to get the metrics (applies to all processes).

Check the README from collectd-monit-alarm-handler for more information about alarm and actuator definitions:
https://gitlab.cern.ch/monitoring/collectd-monit-alarm-handler
